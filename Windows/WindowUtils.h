#ifndef WINDOWSUTILS
#define WINDOWSUTILS

#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <string>
#include <sstream>
#include <assert.h>

#define DBOUT( s )            \
{                             \
   std::ostringstream os_;    \
   os_ << s << "\n";                   \
   OutputDebugString( os_.str().c_str() );  \
}

#define WDBOUT(s)				\
{								\
   std::wostringstream os_;		\
   os_ << s << L"\n";				       \
   OutputDebugStringW( os_.str().c_str() );  \
}

class MyD3D;

/*
* Opening windows, receiving messages from the O.S., basic windows
* house keeping. Running a game Update/Render loop while also responding
* to O.S. messages properly and timing how long a frame update takes
*/
class WinUtil
{
public:
	WinUtil(WinUtil const&) = delete;
	void operator=(WinUtil const&) = delete;
	static WinUtil& Get()
	{ 
		static WinUtil instance; 
		return instance;
	}
	/*
	* create a window we can use for rendering
	* width/height - client resolution of our window
	* hInstance - application handle for our exe
	* appName - write this on the window frame
	* mssgHandler - a function the O.S. can use to send us messages
	* centred - do we want the window in the middle of the desktop
	*/
	bool InitMainWindow(int width, int height, HINSTANCE hInstance, const std::string& appName, WNDPROC mssgHandler, bool centred=true);
	//wrap your game in one function call if using basic functions
	//this one uses high resolution performance counters for accurate timing (more popular)
	int Run(void(*pUpdate)(float), void(*pRender)(float));
	//this one uses the standard template library chrono timing class (still works fine)
	int Run2(void(*pUpdate)(float), void(*pRender)(float));
	//if you are using class member functions then try this instead
	bool BeginLoop(bool& canUpdateRender);
	float EndLoop(bool didUpdateRender);
	//or even rework the Run function using std::function and lamdas to wrap the member function with a this pointer

	HINSTANCE GetAppInst();
	HWND GetMainWnd();
	void GetClientExtents(int& width, int& height);
	void ChooseRes(int& w, int& h, int defaults[], int numPairs);

	void AddSecToClock(float secs) {
		mTimeSecs += secs;
	}
	float GetTime() const {
		return mTimeSecs;
	}
	LRESULT DefaultMssgHandler(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);

private:
	struct WinData
	{
		//all windows apps have these handles
		HINSTANCE hAppInst = 0;
		HWND      hMainWnd = 0;
		bool      appPaused = false;
		bool      minimized = false;
		bool      maximized = false;
		bool      resizing = false;
		std::string mainWndCaption;
		int clientWidth;
		int clientHeight;
	};
	WinData mWinData;
	float mTimeSecs = 0;
	
	WinUtil() {}
};

#endif
