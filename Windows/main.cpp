#include <windows.h>
#include <string>
#include <cassert>
#include <iostream>

#include "WindowUtils.h"

using namespace std;


class Game
{
public:
	static Game& Get()
	{
		static Game instance;
		return instance;
	}
private:
	Game() {
		InitGame();
	}

public:
	Game(const Game&) = delete;
	void operator=(const Game&) = delete;

	//anything that needs to happen just once at the start
	void InitGame()
	{

	}

	//any memory or resources we made need releasing at the end
	void ReleaseGame()
	{

	}

	//called over and over, use it to update game logic
	void Update(float dTime)
	{

	}

	static void SUpdate(float dTime) {
		Game::Get().Update(dTime);
	}

	//called over and over, use it to render things
	void Render(float dTime)
	{

	}
	static void SRender(float dTime) {
		Game::Get().Render(dTime);
	}
	void CheckInput(unsigned char key) {
		switch (key)
		{
		case 27:
		case 'q':
		case 'Q':
			PostQuitMessage(0);
		}
	}
};



//messages come from windows all the time, should we respond to any specific ones?
LRESULT CALLBACK MainWndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	//do something game specific here
	switch (msg)
	{
		// Respond to a keyboard event.
	case WM_CHAR:
		Game::Get().CheckInput(wParam);
		return 0;
	}

	//default message handling (resize window, full screen, etc)
	return WinUtil::Get().DefaultMssgHandler(hwnd, msg, wParam, lParam);
}

//main entry point for the game
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE prevInstance,
				   PSTR cmdLine, int showCmd)
{

	int w(1024), h(768);
	if (!WinUtil::Get().InitMainWindow(w, h, hInstance, "Fezzy", MainWndProc, true))
		assert(false);

	Game::Get();

	bool canUpdateRender;
	float dTime = 0;
	while (WinUtil::Get().BeginLoop(canUpdateRender))
	{
		if (canUpdateRender)
		{
			Game::Get().Update(dTime);
			Game::Get().Render(dTime);
		}
		dTime = WinUtil::Get().EndLoop(canUpdateRender);
	}

	//WinUtil::Get().Run(Game::SUpdate, Game::SRender);
	//WinUtil::Get().Run2(Update, Render);

	Game::Get().ReleaseGame();
	return 0;
}